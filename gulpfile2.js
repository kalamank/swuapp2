var gulp = require('gulp');

var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-ruby-sass');

var libJSFile = [
    './assets/lib/jquery/dist/jquery.min.js',
    './resources/assets/lib/bootstrap/dist/js/bootstrap.min.js',
];

var libCSSFile = [
    './resources/assets/lib/bootstrap/dist/css/bootstrap.min.css'
];



gulp.task ('css',function(){
    return gulp
    .src('./resources/assets/css/*.css')    
    .pipe(minifyCSS())
    .pipe(rename({suffix : '.min'}))
    .pipe(gulp.dest('./wwwroot/css'));
});

gulp.task ('js',function(){
    return gulp
    .src('./resources/assets/js/*.js')    
    .pipe(minifyJS())
    .pipe(rename({suffix : '.min'}))
    .pipe(gulp.dest('./wwwroot/js'));
});

gulp.task ('libjs',function(){
    return gulp
    .src(libJSFile)    
    .pipe(gulp.dest('./wwwroot/mylib/js'));
});

gulp.task ('libcss',function(){
    return gulp
    .src(libCSSFile)    
    .pipe(gulp.dest('./wwwroot/lib/css'));
});


gulp.task ('all',['css','js','libjs','libcss']);
