var gulp = require('gulp');

var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-ruby-sass')



var libjs = [
    './assets/jquery/dist/jquery.min.js',
    './assets/bootstrap/dist/js/bootstrap.min.js'
];

var libcss = [
    './assets/bootstrap/dist/css/bootstrap.min.css'
]

gulp.task('copylibjs', function() {
    return gulp
    .src(libjs)
    .pipe(gulp.dest('wwwroot/js'));
});

gulp.task('copylibcss', function() {
    return gulp
    .src(libcss)
    .pipe(gulp.dest('wwwroot/css'));
});

//ย่อไฟล์
gulp.task('minjs', function() {
    return gulp
    .src('./assets/mylib/js/script.js')
    .pipe(minifyJS(''))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('wwwroot/js'));
});

//compile scss to css แล้วย่อไฟล์
gulp.task('css', function() {
    sass('./assets/mylib/css/style.scss',{
        style: 'compressed'
    })
    .on('error',sass.logError)           //Compile and minify
	.pipe(rename({suffix: '.min'})) //หลัง minify เพิ่ม .min ต่อท้าย
	.pipe(gulp.dest('wwwroot/css')); 	   //dest : คือ Folder ที่ต้องการ Save (ถ้าไม่มีจะ Create)
});

gulp.task ('all',['copylibjs','copylibcss','minjs','css']);